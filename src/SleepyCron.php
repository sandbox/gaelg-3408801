<?php

namespace Drupal\sleepy_cron;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\CronInterface;
use Drupal\Core\Site\Settings;

/**
 * Decorates the core cron service.
 */
class SleepyCron implements CronInterface {

  /**
   * Decorated cron service.
   *
   */
  protected CronInterface $innerCron;

  /**
   * @var \Drupal\sleepy_cron\SleepyCronStatus
   */
  private SleepyCronStatus $status;

  private TimeInterface $time;

  public function __construct(CronInterface $inner_cron, SleepyCronStatus $status, TimeInterface $time) {
    $this->innerCron = $inner_cron;
    $this->status = $status;
    $this->time = $time;
  }

  public function run(): bool {
    if ($this->status->sleepyCronFeatureIsEnabled()
      && $this->status->cronIsAsleep()) {
      // We do not run cron jobs when cron is already asleep.
      return TRUE;
    }

    // If there has been much time* since the latest website usage, it's time to
    // get the cron to sleep.
    // * "much time" defaults to 6 hours.
    $now = $this->time->getRequestTime();
    $time_before_sleeping = Settings::get('sleepy_cron.time_before_sleeping', 21600);
    $get_to_sleep_at = $this->status->getLatestRecordedUsageTime() + $time_before_sleeping;
    if ($this->status->sleepyCronFeatureIsEnabled()
      && $now >= $get_to_sleep_at) {
      $this->status->getCronToSleep();
      // We do not run cron jobs when cron is asleep.
      return TRUE;
    }

    // Normal cron execution.
    return $this->innerCron->run();
  }

}
