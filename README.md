# Sleepy cron

This module aims to preserve server resources by not running the cron when a
website has not been used for a long time.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sleepy_cron).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sleepy_cron).

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

For even better performance, you can avoid even triggering the cron by externally
checking if the cron is asleep, thanks to the cron_is_asleep.txt file.

For example, if you used <code>drush cron</code> calls in your crontab, add a condition
such as this one:
<pre>
if [ ! -f "sites/default/files/cron_is_asleep.txt" ];then php ../vendor/bin/drush cron --yes 2>&1;fi
</pre>

## Configuration

1. Enable the module at Administration > Extend.
1. You can change the default settings in your settings.php file, like this:
<pre>
// Allows to easily disable the feature on some environments such as prod.
$settings['sleepy_cron.enabled'] = FALSE;

// How many seconds with no website usage before we pause cron?
$settings['sleepy_cron.time_before_sleeping'] = 3600;

// Where to put the file that tells that the cron system is currently paused.
// This file can be checked by an external script to avoid even triggering a
// cron execution.
$settings['sleepy_cron.cron_state_file_dir'] = 'private://my_subdir';
</pre>
